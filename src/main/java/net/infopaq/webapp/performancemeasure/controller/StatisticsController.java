package net.infopaq.webapp.performancemeasure.controller;

import com.google.gson.Gson;
import net.infopaq.webapp.performancemeasure.model.Stat;
import net.infopaq.webapp.performancemeasure.repository.SenseTimingEventDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/rest")
public class StatisticsController
{
        @Autowired
        private SenseTimingEventDAO senseTimingEventDAO;

        @RequestMapping("/stat/{name}")
        public @ResponseBody String getAllRequests(@PathVariable("name") String name)
        {
                List<Stat> stats;
                if ("sense".equals(name))
                {
                        stats = senseTimingEventDAO.getAllSenseRequests();
                }
                else
                {
                        stats = senseTimingEventDAO.getAllRequests();
                }


                String statJson = new Gson().toJson(stats);
                return statJson;
        }

}