package net.infopaq.webapp.performancemeasure.repository;

import net.infopaq.webapp.performancemeasure.model.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * User: csj
 * Date: 11/01/2013
 * Time: 5:13 AM
 */
@Repository
public class SenseTimingEventDAO
{
        @Autowired
        private JdbcTemplate jdbcTemplate;

        public List<Stat> getAllRequests()
        {
                return jdbcTemplate.query("select date_format(created, '%Y-%m-%d %H:00:00') Hour, floor(avg(duration)) avg, count(*) count from TimingEvent where action='Duration' group by 1 order by 1 desc",
                        new StatRowMapper()
                );
        }

        public List<Stat> getAllSenseRequests()
        {
                return jdbcTemplate.query("select date_format(created, '%Y-%m-%d %H:00:00') Hour, floor(avg(duration)) avg, count(*) count from TimingEvent where action='Duration' and context not like '%unknown%' group by 1 order by 1 desc",
                        new StatRowMapper()
                );
        }

        private static class StatRowMapper implements RowMapper<Stat>
        {

                @Override
                public Stat mapRow(ResultSet resultSet, int i) throws SQLException
                {
                        Stat stat = new Stat();
                        stat.group = resultSet.getString(1);
                        stat.avg = resultSet.getLong(2);
                        stat.count = resultSet.getLong(3);
                        return stat;

                }
        }

}
