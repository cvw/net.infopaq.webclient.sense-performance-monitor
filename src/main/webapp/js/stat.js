/**
 * Created with IntelliJ IDEA.
 * User: csj
 * Date: 11/01/2013
 * Time: 5:50 AM
 * To change this template use File | Settings | File Templates.
 */
angular.module('statLoader', ['ngResource']).
    factory('StatLoader', function($resource) {
        var StatLoader = $resource("/rest/stat/:name")
        return StatLoader
    })

angular.module('project', ['statLoader']).
    config(function($routeProvider) {
        $routeProvider.
            when('/', {controller:StatCtrl, templateUrl:'statTable.html'}).
            otherwise({redirectTo:'/'});
    });

angular.service('statService', function() {
    this.stats = []
    this.changeStats = function()
    {
        this.stats = StatLoader.query({name: $scope.statName})
    }
});

function StatCtrl($scope, statService) {
    $scope.statName = $watch('statNameWatch', statService.statName);
    $scope.statNames = ["all", "sense"]

    $scope.changeStats = function()
    {
        statService.changeStats(statName)
    }

}
